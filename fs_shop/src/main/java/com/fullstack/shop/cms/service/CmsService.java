package com.fullstack.shop.cms.service;

import org.springframework.stereotype.Service;

import com.fullstack.common.service.BaseService;
import com.fullstack.shop.cms.entity.Cms;

/**
 * 
 * @author chay
 * @version 2017-5-9
 */
@Service
public interface CmsService<T> extends BaseService<Cms> {
	
	
}