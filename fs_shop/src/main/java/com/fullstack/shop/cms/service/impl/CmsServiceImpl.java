package com.fullstack.shop.cms.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.fullstack.common.service.impl.BaseServiceImpl;
import com.fullstack.common.utils.ImgUtils;
import com.fullstack.shop.cms.dao.CmsDao;
import com.fullstack.shop.cms.entity.Cms;
import com.fullstack.shop.cms.service.CmsService;

/**
 * 
 * @author chay
 * @version 2017-5-9
 */
@Service
public class CmsServiceImpl extends BaseServiceImpl<CmsDao, Cms> implements CmsService<Cms>{

	@Override
	public List<Cms> selectList(Wrapper<Cms> wrapper) {
		List<Cms> list = baseMapper.selectList(wrapper);
		for(Cms c : list) {
			c.setPath(ImgUtils.attachPathUtils(c.getId(), Cms.TYPE_BANNER));
		}
		return list;
    }
	

}